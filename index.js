const { Client } = require('pg')

const client = new Client({
    user: 'user',
    host: 'host',
    database: 'db',
    password: 'pass',
    port: 5432,
})

async function addSalaries(ids, employeIds, amounts) {
    await client.connect()
    try {
        await client.query('BEGIN')
        for (let i = 0; i < ids.length; i++) {
            let q = `INSERT INTO Salaries VALUES (${ids[i]},${employeIds[i]},${amounts[i]})`
            await client.query(q);
        }
        await client.query('COMMIT')
    } catch (e) {
        await client.query('ROLLBACK')
        console.log('Rolled back!');
        console.log(e.detail); // throw e
    } finally {
        client.end()
    }

}

async function getSalaries() {
    await client.connect()
    client.query('SELECT * FROM Salaries;', (err, res) => {
        if (err) console.log(err)
        else console.log(res.rows)
        client.end()
    })
}

// getSalaries();

/*
    Rows in Employees same as in lab
    Rows in Salaries:
    { id: 2, employee_id: 107, amount: 24000 }
    { id: 1, employee_id: 106, amount: 12000 }
*/

/*
    Example:
    First INSERT query is valid
    Second violates foreign key constraint (no employee id 200)
*/
addSalaries([3, 4], [108, 200], [36000, 999])
